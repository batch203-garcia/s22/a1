let registerdUserList = ["James Jeffries", "Gunther SMith", "Macie West", "Michelle Queen", "Shane Miguelito", "Fernando Dela Cruz", "Akiko Yukihime"];
let friendList = [];

function register(userFullName) {

    if (registerdUserList.includes(userFullName)) {
        alert("Registration Failed. User already exist!");
    } else {
        registerdUserList.push(userFullName);
        alert("Thank you for registering!")
    }

}

function addFriend(userAddFriend) {
    if (registerdUserList.includes(userAddFriend)) {
        friendList.push(userAddFriend);
        alert("You have added " + userAddFriend + " as a friend.");
    } else {
        alert("User Not Found");
    }
}

function displayFriends() {
    if (friendList.length != 0) {
        for (let i = 0; i < friendList.length; i++) {
            console.log(friendList[i]);
        }
    } else {
        alert("You have 0 friend/s. Add one first.");
    }
}

function displayNumberOfFriends() {
    if (friendList.length != 0) {
        alert("You currently have " + friendList.length + " friends.");
    } else {
        alert("You have 0 friend/s. Add one first.");
    }
}

function deleteFriend() {
    if (friendList.length == 0) {
        alert("You have 0 friend/s. Add one first.");
    } else {
        friendList.pop();
        alert("You successfully deleted a friend on you friend list.");
    }
}